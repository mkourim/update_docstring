# -*- coding: utf-8 -*-
"""
Polarion docstring fields info.
"""


POLARION_FIELDS = {
    "assignee": "",
    "initialEstimate": "",
    "caseimportance": "high",
    "caselevel": "component",
    "caseposneg": "positive",
    "caseautomation": "automated",
    "testtype": "functional",
    "casecomponent": "-",
    "subcomponent": "",
    "subtype1": "-",
    "subtype2": "-",
    "tags": "",
    "setup": "",
    "teardown": "",
    "description": "",
    "linkedWorkItems": "",
    "testSteps": "",
    "expectedResults": "",
    "title": "",
    "startsin": "",
    "endsin": "",
    "upstream": "",
    "customerscenario": "",
    "legacytest": "",
}


# fields valid only for manual test cases
MANUAL_ONLY_FIELDS = (
    "title",
    "setup",
    "teardown",
    "testSteps",
    "expectedResults",
    "startsin",
    "endsin",
)

# fields that need to be present in every docstring
REQUIRED_FIELDS = ("assignee", "initialEstimate")

# mapping caselevel to "tier" marker
CASELEVELS = {"component": "0", "integration": "1", "system": "2", "acceptance": "3"}

# mapping of component names to their internal representation
CASECOMPONENT_MAP = {
    "Cloud": "cloud",
    "Infra": "infra",
    "Services": "services",
    "Control": "control",
    "Automate": "automate",
    "Configuration": "config",
    "Appliance": "appl",
    "C&U": "candu",
    "Reporting": "report",
    "SmartState": "smartst",
    "Provisioning": "prov",
    "SelfServiceUI": "ssui",
    "Stack": "stack",
    "UI": "web_ui",
    "Optimize": "optimize",
    "Ansible": "ansible",
    "V2V": "V2V",
}

# keys with fixed (not free-text) values
FIXED_VALUES = ("caseimportance", "caseposneg", "testtype", "casecomponent", "subcomponent")
